## Test done with laravel 5.7 (php, mysql)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

## Run the bash script (start.sh) to update all dependencies and setup mysql

## Application will start on 127.0.0.1:8080

## Test using postman

	## To create order
	
	for /order endpoint (provide the origin and destination address), the service will geocode and get the appropriate lat and longitude, it the saves in the database and returns the order id, distand and status

	## Listing all orders
	
	/orders?page=:page&limit=:limit endpoint list all the orders

	## Take Order endpoint 
	/order/:id
