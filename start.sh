#!/bin/sh

# Lalamove project setup bash script.
# Inspired by Temidayo Uji

projectname=$1

sudo debconf-set-selections <<< 'mysql-server-5.6 mysql-server/root password root'
sudo debconf-set-selections <<< 'mysql-server-5.6 mysql-server/root password root'
sudo apt-get -y install mysql-server-5.6

mysql -uroot -e "create database lalamove"

echo -e "Mysql table created successfully";

composer install
php artisan migrate
php artisan serve --port=8080

echo -e "############################
Application running on port 8080
############################";