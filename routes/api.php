<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/order', 'api\OrdersController@createOrder');
Route::get('/orders', 'api\OrdersController@listOrders');
Route::get('/orders/{orderId}', 'api\OrdersController@takeOrder');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
