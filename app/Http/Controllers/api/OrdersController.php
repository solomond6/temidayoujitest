<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class OrdersController extends Controller
{
	
	function getDistanceFromLatLonInKm($lat1,$lon1,$lat2,$lon2) {
	  $R = 6371; // Radius of the earth in km
	  $dLat = deg2rad($lat2-$lat1);  // deg2rad below
	  $dLon = deg2rad($lon2-$lon1); 
	  $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * sin($dLon/2) * sin($dLon/2); 
	  $c = 2 * atan2(sqrt($a), sqrt(1-$a)); 
	  $d = $R * $c; // Distance in km
	  return $d;
	}

	function deg2rad($deg) {
	  return $deg * (pi/180);
	}
	

    public function createOrder(Request $request){
    	
    	$key = 'AIzaSyBWOR5z1uSThArfmuNQUUgyWbYW5I5LUqQ';

        try {
        	if($request->isMethod('post')){
	            $data = $request->all();
	            $origin = str_replace(' ', '+', $data['origin']);
	            $destination = str_replace(' ', '+',$data['destination']);

	            $geocode_origin = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$origin.'&key='.$key);

	            $geocode_destination = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$destination.'&key='.$key);

	            $lat_long_origin = json_decode($geocode_origin);
	            $lat_long_destination = json_decode($geocode_destination);
	            
				$lat_origin = $lat_long_origin->results[0]->geometry->location->lat;
				$long_origin = $lat_long_origin->results[0]->geometry->location->lng;
				$lat_destination = $lat_long_destination->results[0]->geometry->location->lat;
				$long_destination = $lat_long_destination->results[0]->geometry->location->lng;

	            $distance = $this->getDistanceFromLatLonInKm($lat_origin, $long_origin, $lat_destination, $long_destination);
	            
	            $order_id = DB::table('orders')->insertGetId([
			            		'origin' => $data['origin'], 
			            		'destination' => $data['destination'], 
			            		'origin_lat_long'=>json_encode([$lat_origin, $long_origin]),
			            		'distination_lat_long'=>json_encode([$lat_destination, $long_destination]),
			            		'distance'=>$distance,
			            		'status'=>0
			            	]);

				return response()->json(['id'=>$order_id, 'distance'=>$distance, 'status'=>'UNASSIGN'], 200);
	                       
	        }else{
				return response()->json(['error'=>'Not Saved'], 500);
	        }

	    } catch (\Exception $e) {
			return response()->json(['error'=>$e->getMessage()], 500);
	    }
    }

        
    public function listOrders(Request $request)
    {
    	try{
    		$queryString = $request->query();
    		if(empty($queryString)){
    			$page = 0;
    			$offset = 0;
    			$limit = 10;
    		}else{
    			$page = (int)$queryString['page'];
    			$limit = (int)$queryString['limit'];
				$offset = $page*$limit;
    		} 

    		$orders = DB::table('orders')
    					->select('id', 'distance', 'status')
    					->skip($offset)
    					->take($limit)
    					->get();
        	return response()->json($orders, 200);
    	}catch (\Exception $e) {
			return response()->json(['error'=>$e->getMessage()], 400);
	    }
        
    }

    public function takeOrder(Request $request, $orderId=null)
    {
        try{
        	if($orderId != null){
        		$order = DB::table('orders')->where('id', $orderId)->first();
        		if($order->status == 0){
        			DB::table('orders')->where('id', $orderId)->update(['status' => 1]);
        			return response()->json(['status'=>'SUCCESS'], 200);
        		}else{
        			return response()->json(['error'=>'ORDER_ALREADY_BEEN_TAKEN'], 409 );
        		}
        	}else{
        		return response()->json(['error'=>'Order Not Available'], 500);
        	}
    		
    	}catch (\Exception $e) {
			return response()->json(['error'=>$e->getMessage()], 500);
	    }
    }
}
